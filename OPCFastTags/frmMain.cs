﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OPCFastTags.Library;
using System.IO;

namespace OPCFastTags
{
    public partial class frmMain : Form
    {
        #region Properties
        private Processor _Proc;
        public Processor Proc
        {
            get
            {
                if (_Proc == null)
                    _Proc = new Processor();
                return _Proc;
            }
        }
        #endregion

        public frmMain()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Proc.CloseConnection();
            System.Environment.Exit(System.Environment.ExitCode);
        }

        private void btnAddTags_Click(object sender, EventArgs e)
        {
            frmBrowseTags Browser = null;
            try
            {
                Browser = new frmBrowseTags(Proc);
                Browser.SelectedTags += Browser_SelectedTags;
                Browser.ShowDialog();
            }
            catch (Exception ex)
            {
                CabZFramework.Core.CommonModule.ExceptionFunc(
                    this.GetType().ToString(), "btnAddTags_Click", ex, true);
            }
            finally
            {
                if (Browser != null)
                    Browser.Dispose();
                Browser = null;
            }
        }

        #region Selected Tags
        private DataGridViewRow FindInSelectedTags(OPCObjectWrapper o)
        {
            try
            {
                if (dgvTags.RowCount > 0 && o != null)
                {
                    foreach (DataGridViewRow row in dgvTags.Rows)
                    {
                        OPCObjectWrapper RowTag = (OPCObjectWrapper)row.Tag;
                        if (RowTag != null && string.Compare(RowTag.OPCObject.InstanceId.FullyQualifiedId,
                            o.OPCObject.InstanceId.FullyQualifiedId, true) == 0)
                            return row;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return null;
        }

        private void Browser_SelectedTags(params OPCObjectWrapper[] tags)
        {            
            try
            {
                if (tags != null && tags.Length > 0)
                { 
                    foreach(OPCObjectWrapper tag in tags)
                    {
                        if (FindInSelectedTags(tag) == null)
                        {
                            int RowIndex = dgvTags.Rows.Add();
                            DataGridViewRow Row = dgvTags.Rows[RowIndex];
                            Row.Cells["tagName"].Value = tag.OPCObject.InstanceId.FullyQualifiedId;
                            Row.Cells["tagDesc"].Value = tag.OPCObject.Description;
                            Row.Tag = tag;
                        }
                    }
                    dgvTags.ClearSelection();
                }
                btnRun.Enabled = tags != null && tags.Length > 0;
            }
            catch (Exception ex)
            {
                CabZFramework.Core.CommonModule.ExceptionFunc(
                        this.GetType().ToString(), "Browser_SelectedTags", ex, true);
            }
        }
        #endregion

        private void dgvTags_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    if (string.Compare(dgvTags.Columns[e.ColumnIndex].Name, "btnDelSelected", true) == 0)
                    {
                        dgvTags.Rows.Remove(dgvTags.Rows[e.RowIndex]);
                        if (dgvTags.RowCount <= 0)
                            btnRun.Enabled = false;
                    }
                }
            }
            catch(Exception ex)
            {
                CabZFramework.Core.CommonModule.ExceptionFunc(
                        this.GetType().ToString(), "dgvTags_CellClick", ex, true);
            }
        }

        private void dgvTags_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvTags.SelectedRows == null || dgvTags.SelectedRows.Count <= 0)
                propGrid.SelectedObject = null;
            else if (dgvTags.SelectedRows[0].Tag != null)
            {
                propGrid.SelectedObject = ((OPCObjectWrapper)dgvTags.SelectedRows[0].Tag).Settings;
            }
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
            GC.Collect();
        }

        private void propGrid_SelectedObjectsChanged(object sender, EventArgs e)
        {
            propGrid.Refresh();
        }

        private void btnClearTags_Click(object sender, EventArgs e)
        {
            if (dgvTags.RowCount > 0)
                dgvTags.Rows.Clear();
            btnRun.Enabled = false;
        }

        private void frmMain_Shown(object sender, EventArgs e)
        {
            try
            {
                if (Proc.Config.FetchData() <= 0)
                {
                    using (frmConfig ConfigForm = new frmConfig(Proc.Config, Proc))
                    {
#if DEBUG
                        Proc.Config.ip_server = "127.0.0.1";
                        Proc.Config.server_name = "Matrikon.OPC.Simulation.1";

#endif
                        if (ConfigForm.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
                            this.Close();
                    }
                }

                UIHelper.InitCBDuration(cbSmallDuration);
                UIHelper.InitCBDuration(cbBigDuration);

                cbSmallDuration.SelectedValue = (int)UIHelper.DurationType.Hour;
                cbBigDuration.SelectedValue = (int)UIHelper.DurationType.Day;

                dtEnd.Value = DateTime.ParseExact(string.Format("{0} 07:00:00", DateTime.Now.ToString("MM/dd/yyyy")), "MM/dd/yyyy HH:mm:ss", null);
                dtStart.Value = dtEnd.Value.Subtract(new TimeSpan(1, 0, 0, 0));
            }
            catch(Exception ex)
            {
                CabZFramework.Core.CommonModule.ExceptionFunc(this.GetType().ToString(), "frmMain_Shown", ex, true);
            }
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            try
            {
                using (frmConfig ConfigForm = new frmConfig(Proc.Config, Proc))
                {
                    ConfigForm.ShowDialog();
                }
            }
            catch(Exception ex)
            {
                CabZFramework.Core.CommonModule.ExceptionFunc(this.GetType().ToString(), "btnConfig_Click", ex, true);
            }
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvTags.RowCount > 0)
                {
                    List<OPCObjectWrapper> Tags = new List<OPCObjectWrapper>();
                    foreach (DataGridViewRow row in dgvTags.Rows)
                    {
                        ((OPCObjectWrapper)row.Tag).DecimalPoints = (int)nmDecimalPoint.Value;
                        Tags.Add((OPCObjectWrapper)row.Tag);
                        Tags[Tags.Count - 1].Settings.ProcessorObj = Proc;
                    }

                    using (frmTags ShowData = new frmTags(Tags, dtStart.Value, dtEnd.Value,
                            (int)nmDecimalPoint.Value,
                            Processor.CalculateSeconds((int)nmSmallStep.Value, (UIHelper.DurationType)cbSmallDuration.SelectedValue),
                            Processor.CalculateSeconds((int)nmBigStep.Value, (UIHelper.DurationType)cbBigDuration.SelectedValue)))
                    {
                        ShowData.ShowDialog();
                    }
                    dgvTags_SelectionChanged(this, null);
                }
            }
            catch(Exception ex)
            {
                CabZFramework.Core.CommonModule.ExceptionFunc(this.GetType().ToString(), "btnRun_Click", ex, true);
            }
        }
    }
}
