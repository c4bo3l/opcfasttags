﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OPCFastTags.Library;
using CabZFramework.WIN;
using dotnetCHARTING.WinForms;
using Advosol.Paxi;
using CabZFramework;
using CabZFramework.Core;

namespace OPCFastTags
{
    public partial class frmTags : Form
    {
        #region Properties
        private List<OPCObjectWrapper> _TagCol;
        private DateTime _StartTime, _EndTime;
        private int _DecimalPoints;
        private int _SmallSeconds, _BigSeconds;

        private frmWaiting _WaitForm;
        private frmWaiting WaitForm
        {
            get
            {
                if (_WaitForm == null)
                    _WaitForm = new frmWaiting();
                return _WaitForm;
            }
            set
            {
                _WaitForm = value;
            }
        }

        private List<KeyValuePair<OPCObjectWrapper, Series>> _MainTagsData;
        private List<KeyValuePair<OPCObjectWrapper, Series>> MainTagsData
        {
            get
            {
                if (_MainTagsData == null)
                    _MainTagsData = new List<KeyValuePair<OPCObjectWrapper, Series>>();
                return _MainTagsData;
            }
        }
        #endregion

        #region Constructor
        private frmTags()
        {
            InitializeComponent();
        }

        public frmTags(List<OPCObjectWrapper> tags, DateTime startTime, DateTime endTime, 
            int decimalPoints, int smallSeconds, int bigSeconds)
            : this()
        {
            _TagCol = tags;
            _StartTime = startTime;
            _EndTime = endTime;
            _DecimalPoints = decimalPoints;
            _SmallSeconds = smallSeconds;
            _BigSeconds = bigSeconds;
        }
        #endregion

        #region Enable / Disbale Toolstrip buttons
        public delegate void EnabledChange(bool enable);
        public void EnableDisableToolstripButton(bool enable)
        {
            if (toolStrip.InvokeRequired)
                toolStrip.Invoke(new EnabledChange(EnableDisableToolstripButton), enable);
            else
                toolStrip.Enabled = enable;
        }
        #endregion

        #region Change progress bar value
        public void ChangeProgressValues(int value)
        {
            if (prgStatus.ProgressBar.InvokeRequired)
                prgStatus.ProgressBar.Invoke(new Processor.ChangeProgress(ChangeProgressValues), value);
            else
            {
                prgStatus.ProgressBar.Value = value;
            }
        }
        #endregion

        #region Change status text
        public void ChangeStatus(string status)
        {
            lblStatus.Text = status;
        }
        #endregion

        #region Create Tab Page
        protected delegate void DelegateTabPage(OPCObjectWrapper o);
        protected void AddTabPage(OPCObjectWrapper o)
        { 
            try
            {
                if (tcTags.InvokeRequired)
                    tcTags.Invoke(new DelegateTabPage(AddTabPage), o);
                else
                {
                    TabPage TPage = new TabPage(o.OPCObject.InstanceId.FullyQualifiedId);
                    tcTags.TabPages.Add(TPage);
                    
                    TabPage SelectedTabPage = TPage;
                    if (o.Settings.RegressionTags != null && o.Settings.RegressionTags.RegTags != null && o.Settings.RegressionTags.RegTags.Count > 0)
                    {
                        TabControl NewTabControl = new TabControl();
                        NewTabControl.Name = string.Format("{0}_pages", o.OPCObject.Name);
                        NewTabControl.Dock = DockStyle.Fill;
                        TPage.Controls.Add(NewTabControl);

                        SelectedTabPage = new TabPage(string.Format("{0} data", o.OPCObject.Name));
                        SelectedTabPage.Name = string.Format("{0}_page", o.OPCObject.Name);
                        NewTabControl.TabPages.Add(SelectedTabPage);

                        foreach (OPCObjectWrapper regTag in o.Settings.RegressionTags.RegTags)
                        {
                            TagControl RegTControl = new TagControl(o, Application.StartupPath);
                            RegTControl.ChartOutputType += TControl_ChartOutputType;
                            RegTControl.Dock = DockStyle.Fill;
                            RegTControl.btnCSV.Visible = false;

                            RegTControl.chTag.Type = dotnetCHARTING.WinForms.ChartType.Scatter;
                            RegTControl.chTag.Use3D = false;

                            RegTControl.chTag.XAxis.Label.Text = o.OPCObject.Name;
                            RegTControl.chTag.YAxis.Label.Text = regTag.OPCObject.Name;

                            Series ScatterSeries = new Series();
                            ScatterSeries.Type = SeriesType.Marker;
                            ScatterSeries.Name = "Mapped points";

                            List<double> NewDataValue = new List<double>();
                            List<KeyValuePair<double, double>> BaseData = regTag.Data.Select(d => 
                                new KeyValuePair<double, double>(d.TimeStamp.ToOADate(), (double)d.Value)).ToList();

                            double LowestX = 999999, HighestX = -999999;
                            foreach (DataValue val in o.Data)
                            {
                                double InterpolatedData = Statistics.Interpolation(val.TimeStamp.ToOADate(), BaseData);
                                Element ScatterElement = new Element();
                                ScatterElement.XValue = (double)val.Value;
                                ScatterElement.YValue = InterpolatedData;
                                NewDataValue.Add(InterpolatedData);

                                ScatterSeries.Elements.Add(ScatterElement);

                                if ((double)val.Value < LowestX)
                                    LowestX = (double)val.Value;

                                if ((double)val.Value > HighestX)
                                    HighestX = (double)val.Value;
                            }

                            double Slope = Statistics.Slope(o.Data.Select(d => (double)d.Value).ToArray(), 
                                NewDataValue.ToArray());
                            double Intercept = Statistics.Intercept(Slope, o.Data.Select(d => 
                                (double)d.Value).FirstOrDefault(), NewDataValue[0]);

                            RegTControl.chTag.Title = string.Format("{0} vs {1} between {2} and {3}. Correlation coefficient : {4}",
                                o.OPCObject.Name, regTag.OPCObject.Name, _StartTime.ToString("MMM dd yyyy HH:mm:ss"),
                                _EndTime.ToString("MMM dd yyyy HH:mm:ss"), 
                                Math.Round(Statistics.CorrelationCoefficient(o.Data.Select(d => (double)d.Value).ToArray(),
                                NewDataValue.ToArray()), _DecimalPoints, MidpointRounding.AwayFromZero));

                            Series LineSeries = new Series();

                            LineSeries.Type = SeriesType.Line;

                            LineSeries.AddElements(new Element("", LowestX, 
                                Statistics.CalculateFromLineEquation(Slope, Intercept, LowestX)));
                            LineSeries.AddElements(new Element("", 
                                HighestX, Statistics.CalculateFromLineEquation(Slope, Intercept, HighestX)));

                            RegTControl.chTag.SeriesCollection.Add(ScatterSeries);
                            RegTControl.chTag.SeriesCollection.Add(LineSeries);
                            RegTControl.chTag.LegendBox.Visible = false;

                            TabPage RegTabPage = new TabPage(string.Format("{0} vs {1}", o.OPCObject.Name, regTag.OPCObject.Name));
                            RegTabPage.Name = string.Format("reg_{0}_{1}", o.OPCObject.Name, regTag.OPCObject.Name);
                            RegTabPage.Controls.Add(RegTControl);

                            NewTabControl.TabPages.Add(RegTabPage);
                        }
                    }

                    TagControl TControl = new TagControl(o, Application.StartupPath);
                    TControl.DrawChart();
                    MainTagsData.Add(new KeyValuePair<OPCObjectWrapper, Series>(o, TControl.chTag.SeriesCollection[0]));
                    TControl.ChartOutputType += TControl_ChartOutputType;
                    TControl.Dock = DockStyle.Fill;
                    SelectedTabPage.Controls.Add(TControl);
                    
                    tcTags.PerformLayout();
                }
            }
            catch(Exception ex)
            {
                CabZFramework.Core.CommonModule.ExceptionFunc(this.GetType().ToString(), "AddTabPage", ex, false);
            }
        }

        protected delegate void DelegateTabPage2(TabPage tp, int index);
        protected void AddTabPage(TabPage tp, int index)
        {
            if (tcTags.InvokeRequired)
                tcTags.Invoke(new DelegateTabPage2(AddTabPage), tp, index);
            else
            {
                tcTags.TabPages.Insert(index, tp);
                tcTags.SelectedIndex = 0;
            }
        }

        private void TControl_ChartOutputType(int filterIndex)
        {
            try
            {
                if (tcTags.TabPages.Count > 0)
                {
                    foreach(TabPage page in tcTags.TabPages)
                    {
                        foreach (Control ctrl in page.Controls)
                        { 
                            if(ctrl.GetType().Equals(typeof(TagControl)))
                            {
                                ((TagControl)ctrl).saveChart.FilterIndex = filterIndex;
                                break;
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                CabZFramework.Core.CommonModule.ExceptionFunc(this.GetType().ToString(), "TControl_ChartOutputType", ex, false);
            }
        }

        public delegate void DelegateClearTabPage();
        protected void ClearTabPage()
        {
            if (tcTags.InvokeRequired)
                tcTags.Invoke(new DelegateClearTabPage(ClearTabPage));
            else
                tcTags.TabPages.Clear();
        }

        public delegate void EmptyParameter();
        protected void CloseForm()
        {
            if (this.InvokeRequired)
                this.Invoke(new EmptyParameter(CloseForm));
            else
                this.Close();
        }
        #endregion

        #region Run
        protected void Run()
        {
            try
            {
                DateTime StartProcess = DateTime.Now;
                ChangeProgressValues(0);

                ClearTabPage();
                MainTagsData.Clear();
                if (_TagCol != null && _TagCol.Count > 0)
                {
                    _TagCol[0].Settings.ProcessorObj.NewProgress += ChangeProgressValues;
                    _TagCol[0].Settings.ProcessorObj.NewStatus += ChangeStatus;

                    _TagCol[0].Settings.ProcessorObj.FetchData(_TagCol, _StartTime, _EndTime, _DecimalPoints);

                    bool HasData = false;
                    foreach (OPCObjectWrapper o in _TagCol)
                    {
                        if (o.Data != null && o.Data.Count > 0)
                        {
                            AddTabPage(o);
                            HasData = true;
                        }
                    }

                    if (!HasData)
                        throw new NullReferenceException("No data to be displayed");
                    else if (tcTags.TabPages.Count > 1 && HasData)
                    {
                        TagControl SummaryControl = new TagControl(null, Application.StartupPath);
                        SummaryControl.ChartOutputType += TControl_ChartOutputType;
                        SummaryControl.Dock = DockStyle.Fill;
                        SummaryControl.btnCSV.Visible = false;

                        SummaryControl.chTag.Type = dotnetCHARTING.WinForms.ChartType.Combo;
                        SummaryControl.chTag.Use3D = false;
                        SummaryControl.chTag.Title = "Summary";
                        SummaryControl.chTag.LegendBox.DefaultEntry.Name = "%Name";
                        SummaryControl.chTag.LegendBox.Visible = false;
                        SummaryControl.chTag.LegendBox.Orientation = dotnetCHARTING.WinForms.Orientation.Top;

                        foreach (KeyValuePair<OPCObjectWrapper, Series> kv in MainTagsData)
                        {
                            kv.Value.Name = kv.Key.OPCObject.Name;
                            ChartArea NewChart = new ChartArea(kv.Value);
                            NewChart.Title = kv.Key.OPCObject.InstanceId.FullyQualifiedId;
                            NewChart.DefaultElement.Marker.Visible = false;
                            NewChart.DefaultSeries.Type = SeriesType.Line;
                            NewChart.YAxis.ExtraTicks.Add(new AxisTick(0, ""));
                            SummaryControl.chTag.ExtraChartAreas.Add(NewChart);
                        }

                        SummaryControl.chTag.YAxis.ExtraTicks.Add(new AxisTick(0, ""));

                        SummaryControl.chTag.XAxis.TimeInterval = TimeInterval.Seconds;
                        SummaryControl.chTag.XAxis.TimeScaleLabels.RangeMode = TimeScaleLabelRangeMode.Dynamic;
                        SummaryControl.chTag.XAxis.TimeScaleLabels.Mode = TimeScaleLabelMode.Hidden;
                        SummaryControl.chTag.XAxis.TimeScaleLabels.DayTick.Label.Text = "";

                        SummaryControl.chTag.XAxis.Label.Text = "Timestamp";
                        SummaryControl.chTag.XAxis.DefaultTick.Label.Font = new Font("Arial", 10, FontStyle.Bold);
                        SummaryControl.chTag.XAxis.Orientation = dotnetCHARTING.WinForms.Orientation.Bottom;

                        TabPage SummaryPage = new TabPage("Summary");
                        SummaryPage.Name = "summary_page";
                        SummaryPage.Controls.Add(SummaryControl);

                        AddTabPage(SummaryPage, 0);
                    }

                    EnableDisableToolstripButton(true);
                    _TagCol[0].Settings.ProcessorObj.NewProgress -= ChangeProgressValues;
                    _TagCol[0].Settings.ProcessorObj.NewStatus -= ChangeStatus;

                    ChangeProgressValues(100);
                    ChangeStatus(string.Format("Finish in {0} seconds", DateTime.Now.Subtract(StartProcess).TotalSeconds));
                }
            }
            catch(Exception ex)
            {
                CabZFramework.Core.CommonModule.ExceptionFunc(
                        this.GetType().ToString(), "Run", ex, true);
                _TagCol[0].Settings.ProcessorObj.NewProgress -= ChangeProgressValues;
                _TagCol[0].Settings.ProcessorObj.NewStatus -= ChangeStatus;
                bgWorker.CancelAsync();
                CloseForm();
            }
        }
        #endregion

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmTags_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            { this.Dispose(); }
            catch (Exception)
            { }
            finally
            { GC.Collect(); }
        }

        private void frmTags_Shown(object sender, EventArgs e)
        {
            try
            {
                btnCSV.Visible = _TagCol.Count > 1;
                bgWorker.RunWorkerAsync();
            }
            catch(Exception ex)
            {
                CabZFramework.Core.CommonModule.ExceptionFunc(
                        this.GetType().ToString(), "frmTags_Shown", ex, true);
                bgWorker.CancelAsync();
            }
        }

        private void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                EnableDisableToolstripButton(false);
                Run();
            }
            catch (Exception ex)
            {
                CabZFramework.Core.CommonModule.ExceptionFunc(
                        this.GetType().ToString(), "bgWorker_DoWork", ex, true);
                bgWorker.CancelAsync();
            }
        }

        private void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (_WaitForm != null)
                {
                    WaitForm.Close();
                    WaitForm.Dispose();
                    WaitForm = null;
                }
            }
            catch (Exception ex)
            {
                CabZFramework.Core.CommonModule.ExceptionFunc(
                        this.GetType().ToString(), "bgWorker_RunWorkerCompleted", ex, true);
            }
        }

        private void btnMinSmall_Click(object sender, EventArgs e)
        {
            try
            {
                TimeSpan DifTime = _EndTime.Subtract(_StartTime);
                _StartTime = _StartTime.Subtract(new TimeSpan(0, 0, _SmallSeconds));
                _EndTime = _StartTime.Add(DifTime);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                CabZFramework.Core.CommonModule.ExceptionFunc(
                        this.GetType().ToString(), "btnMinSmall_Click", ex, true);
                bgWorker.CancelAsync();
            }
        }

        private void btnMinBig_Click(object sender, EventArgs e)
        {
            try
            {
                TimeSpan DifTime = _EndTime.Subtract(_StartTime);
                _StartTime = _StartTime.Subtract(new TimeSpan(0, 0, _BigSeconds));
                _EndTime = _StartTime.Add(DifTime);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                CabZFramework.Core.CommonModule.ExceptionFunc(
                        this.GetType().ToString(), "btnMinBig_Click", ex, true);
                bgWorker.CancelAsync();
            }
        }

        private void btnPlusSmall_Click(object sender, EventArgs e)
        {
            try
            {
                TimeSpan DifTime = _EndTime.Subtract(_StartTime);
                _StartTime = _StartTime.Add(new TimeSpan(0, 0, _SmallSeconds));
                _EndTime = _StartTime.Add(DifTime);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                CabZFramework.Core.CommonModule.ExceptionFunc(
                        this.GetType().ToString(), "btnPlusSmall_Click", ex, true);
                bgWorker.CancelAsync();
            }
        }

        private void btnPlusBig_Click(object sender, EventArgs e)
        {
            try
            {
                TimeSpan DifTime = _EndTime.Subtract(_StartTime);
                _StartTime = _StartTime.Add(new TimeSpan(0, 0, _BigSeconds));
                _EndTime = _StartTime.Add(DifTime);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                CabZFramework.Core.CommonModule.ExceptionFunc(
                        this.GetType().ToString(), "btnPlusBig_Click", ex, true);
                bgWorker.CancelAsync();
            }
        }

        private void btnCSV_VisibleChanged(object sender, EventArgs e)
        {
            csvSeparator.Visible = btnCSV.Visible;
        }

        private void mnExportToFolder_Click(object sender, EventArgs e)
        {
            try
            { 
                if(dlgFolder.ShowDialog()==System.Windows.Forms.DialogResult.OK)
                {
                    StringBuilder CSVString = new StringBuilder();
                    string OneFilePath = "";
                    foreach (KeyValuePair<OPCObjectWrapper, Series> kv in MainTagsData)
                    {
                        CSVString.Clear();
                        string Path = string.Format("{0}\\{1}_{2}_{3}.csv", dlgFolder.SelectedPath, TagControl.CleanFilename(kv.Key.OPCObject.Name), 
                            _StartTime.ToString("MMddyyyyHHmmss"), _EndTime.ToString("MMddyyyyHHmmss"));

                        if (OneFilePath.Length <= 0)
                            OneFilePath = Path;

                        CSVString.AppendFormat("Tag Name,{0}\n", kv.Key.OPCObject.InstanceId.FullyQualifiedId);
                        CSVString.AppendFormat("Tag Desc,{0}\n", kv.Key.OPCObject.Description);
                        CSVString.AppendFormat("Start Date,{0}\n", kv.Key.Data.Select(d => d.TimeStamp).Min().ToString("MM/dd/yyyy HH:mm:ss"));
                        CSVString.AppendFormat("End Date,{0}\n", kv.Key.Data.Select(d => d.TimeStamp).Max().ToString("MM/dd/yyyy HH:mm:ss"));
                        CSVString.AppendFormat("Count,{0}\n", kv.Key.Data.Count);
                        CSVString.AppendFormat("Min,{0}\n", kv.Key.Min);
                        CSVString.AppendFormat("Max,{0}\n", kv.Key.Max);
                        CSVString.AppendFormat("Mean,{0}\n", kv.Key.Average);
                        CSVString.AppendFormat("Std. Deviation,{0}\n", kv.Key.StandardDeviation);
                        CSVString.AppendFormat("Std. Error,{0}\n", kv.Key.StandardError);
                        CSVString.Append("Timestamp,Value\n");
                        foreach (DataValue value in kv.Key.Data)
                            CSVString.AppendFormat("{0},{1}\n", value.TimeStamp.ToString("MM/dd/yyyy HH:mm:ss"), value.Value.ToString());

                        System.IO.File.WriteAllText(Path, CSVString.ToString());
                    }

                    if (VDialog.Show(this, "Do you want to open the folder?", "Open Folder Confirmation", 
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question, 
                        MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
                        System.Diagnostics.Process.Start("explorer.exe", string.Format("/select,\"{0}\"", OneFilePath));
                }
            }
            catch(Exception ex)
            {
                CabZFramework.Core.CommonModule.ExceptionFunc(
                        this.GetType().ToString(), "mnExportToFolder_Click", ex, true);
            }
        }

        private void mnAppend_Click(object sender, EventArgs e)
        {
            try
            {
                if (csvDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    StringBuilder CSVString = new StringBuilder();

                    CSVString.AppendFormat("Start Date,{0}\n", _StartTime.ToString("MM/dd/yyyy HH:mm:ss"));
                    CSVString.AppendFormat("End Date,{0}\n", _EndTime.ToString("MM/dd/yyyy HH:mm:ss"));
                    CSVString.Append("Tag Name,Timestamp,Value\n");
                    
                    foreach (KeyValuePair<OPCObjectWrapper, Series> kv in MainTagsData)
                    {
                        foreach (DataValue value in kv.Key.Data)
                            CSVString.AppendFormat("{0},{1},{2}\n", kv.Key.OPCObject.InstanceId.FullyQualifiedId, 
                                value.TimeStamp.ToString("MM/dd/yyyy HH:mm:ss"), 
                                value.Value.ToString());
                    }

                    System.IO.File.WriteAllText(csvDialog.FileName, CSVString.ToString());

                    if (VDialog.Show(this, "Do you want to open the file?", "Open File Confirmation",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                        MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
                        System.Diagnostics.Process.Start(csvDialog.FileName);
                }
            }
            catch (Exception ex)
            {
                CabZFramework.Core.CommonModule.ExceptionFunc(
                        this.GetType().ToString(), "mnAppend_Click", ex, true);
            }
        }

        private void mnSideBySide_Click(object sender, EventArgs e)
        {
            try
            {
                if (csvDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    StringBuilder CSVString = new StringBuilder();

                    CSVString.AppendFormat("Start Date,{0}\n", _StartTime.ToString("MM/dd/yyyy HH:mm:ss"));
                    CSVString.AppendFormat("End Date,{0}\n", _EndTime.ToString("MM/dd/yyyy HH:mm:ss"));

                    for (int i = 1; i <= MainTagsData.Count; i++)
                        CSVString.Append("Tag Name,Timestamp,Value,");
                    CSVString.Append("\n");

                    int MaxCount = MainTagsData.Select(m => m.Value.Elements.Count).Max();

                    for (int i = 0; i < MaxCount; i++)
                    {
                        string SubString = "";
                        foreach (KeyValuePair<OPCObjectWrapper,Series> kv in MainTagsData)
                        {
                            string TagName = "", Timestamp = "", Value = "";

                            if (i < kv.Value.Elements.Count)
                            {
                                TagName = kv.Key.OPCObject.InstanceId.FullyQualifiedId;
                                Timestamp = kv.Value.Elements[i].XDateTime.ToString("MM/dd/yyyy HH:mm:ss");
                                Value = kv.Value.Elements[i].YValue.ToString();
                            }

                            SubString = string.Format("{0},{1},{2},{3}", SubString, TagName, Timestamp, Value);
                        }

                        CSVString.AppendFormat("{0}\n", SubString.Substring(1));
                    }

                    System.IO.File.WriteAllText(csvDialog.FileName, CSVString.ToString());

                    if (VDialog.Show(this, "Do you want to open the file?", "Open File Confirmation",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                        MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
                        System.Diagnostics.Process.Start(csvDialog.FileName);
                }
            }
            catch (Exception ex)
            {
                CabZFramework.Core.CommonModule.ExceptionFunc(
                        this.GetType().ToString(), "mnAppend_Click", ex, true);
            }
        }
    }
}
