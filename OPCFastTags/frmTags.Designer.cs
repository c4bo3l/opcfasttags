﻿namespace OPCFastTags
{
    partial class frmTags
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTags));
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.btnCSV = new System.Windows.Forms.ToolStripSplitButton();
            this.mnExportToFolder = new System.Windows.Forms.ToolStripMenuItem();
            this.mnAppend = new System.Windows.Forms.ToolStripMenuItem();
            this.mnSideBySide = new System.Windows.Forms.ToolStripMenuItem();
            this.csvSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.btnMinBig = new System.Windows.Forms.ToolStripButton();
            this.btnMinSmall = new System.Windows.Forms.ToolStripButton();
            this.btnPlusSmall = new System.Windows.Forms.ToolStripButton();
            this.btnPlusBig = new System.Windows.Forms.ToolStripButton();
            this.btnExit = new System.Windows.Forms.ToolStripButton();
            this.tcTags = new System.Windows.Forms.TabControl();
            this.bgWorker = new System.ComponentModel.BackgroundWorker();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.prgStatus = new System.Windows.Forms.ToolStripProgressBar();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.dlgFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.csvDialog = new System.Windows.Forms.SaveFileDialog();
            this.toolStrip.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip
            // 
            this.toolStrip.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnCSV,
            this.csvSeparator,
            this.toolStripLabel1,
            this.btnMinBig,
            this.btnMinSmall,
            this.btnPlusSmall,
            this.btnPlusBig,
            this.btnExit});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(784, 70);
            this.toolStrip.TabIndex = 0;
            this.toolStrip.Text = "toolStrip1";
            // 
            // btnCSV
            // 
            this.btnCSV.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnExportToFolder,
            this.mnAppend,
            this.mnSideBySide});
            this.btnCSV.Image = ((System.Drawing.Image)(resources.GetObject("btnCSV.Image")));
            this.btnCSV.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCSV.Name = "btnCSV";
            this.btnCSV.Size = new System.Drawing.Size(174, 67);
            this.btnCSV.Text = "Export all data to CSV format";
            this.btnCSV.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnCSV.VisibleChanged += new System.EventHandler(this.btnCSV_VisibleChanged);
            // 
            // mnExportToFolder
            // 
            this.mnExportToFolder.Image = ((System.Drawing.Image)(resources.GetObject("mnExportToFolder.Image")));
            this.mnExportToFolder.Name = "mnExportToFolder";
            this.mnExportToFolder.Size = new System.Drawing.Size(278, 38);
            this.mnExportToFolder.Text = "Export to separate files in one folder";
            this.mnExportToFolder.Click += new System.EventHandler(this.mnExportToFolder_Click);
            // 
            // mnAppend
            // 
            this.mnAppend.Image = ((System.Drawing.Image)(resources.GetObject("mnAppend.Image")));
            this.mnAppend.Name = "mnAppend";
            this.mnAppend.Size = new System.Drawing.Size(278, 38);
            this.mnAppend.Text = "Export appended data";
            this.mnAppend.Click += new System.EventHandler(this.mnAppend_Click);
            // 
            // mnSideBySide
            // 
            this.mnSideBySide.Image = ((System.Drawing.Image)(resources.GetObject("mnSideBySide.Image")));
            this.mnSideBySide.Name = "mnSideBySide";
            this.mnSideBySide.Size = new System.Drawing.Size(278, 38);
            this.mnSideBySide.Text = "Export data in side by side format";
            this.mnSideBySide.Click += new System.EventHandler(this.mnSideBySide_Click);
            // 
            // csvSeparator
            // 
            this.csvSeparator.Name = "csvSeparator";
            this.csvSeparator.Size = new System.Drawing.Size(6, 70);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(75, 67);
            this.toolStripLabel1.Text = "Time control";
            // 
            // btnMinBig
            // 
            this.btnMinBig.Image = ((System.Drawing.Image)(resources.GetObject("btnMinBig.Image")));
            this.btnMinBig.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnMinBig.Name = "btnMinBig";
            this.btnMinBig.Size = new System.Drawing.Size(107, 67);
            this.btnMinBig.Text = "Big step backward";
            this.btnMinBig.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnMinBig.Click += new System.EventHandler(this.btnMinBig_Click);
            // 
            // btnMinSmall
            // 
            this.btnMinSmall.Image = ((System.Drawing.Image)(resources.GetObject("btnMinSmall.Image")));
            this.btnMinSmall.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnMinSmall.Name = "btnMinSmall";
            this.btnMinSmall.Size = new System.Drawing.Size(119, 67);
            this.btnMinSmall.Text = "Small step backward";
            this.btnMinSmall.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnMinSmall.Click += new System.EventHandler(this.btnMinSmall_Click);
            // 
            // btnPlusSmall
            // 
            this.btnPlusSmall.Image = ((System.Drawing.Image)(resources.GetObject("btnPlusSmall.Image")));
            this.btnPlusSmall.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPlusSmall.Name = "btnPlusSmall";
            this.btnPlusSmall.Size = new System.Drawing.Size(109, 67);
            this.btnPlusSmall.Text = "Small step forward";
            this.btnPlusSmall.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnPlusSmall.Click += new System.EventHandler(this.btnPlusSmall_Click);
            // 
            // btnPlusBig
            // 
            this.btnPlusBig.Image = ((System.Drawing.Image)(resources.GetObject("btnPlusBig.Image")));
            this.btnPlusBig.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPlusBig.Name = "btnPlusBig";
            this.btnPlusBig.Size = new System.Drawing.Size(97, 67);
            this.btnPlusBig.Text = "Big step forward";
            this.btnPlusBig.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnPlusBig.Click += new System.EventHandler(this.btnPlusBig_Click);
            // 
            // btnExit
            // 
            this.btnExit.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(52, 67);
            this.btnExit.Text = "Exit";
            this.btnExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // tcTags
            // 
            this.tcTags.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcTags.Location = new System.Drawing.Point(0, 70);
            this.tcTags.Name = "tcTags";
            this.tcTags.SelectedIndex = 0;
            this.tcTags.Size = new System.Drawing.Size(784, 470);
            this.tcTags.TabIndex = 3;
            // 
            // bgWorker
            // 
            this.bgWorker.WorkerSupportsCancellation = true;
            this.bgWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorker_DoWork);
            this.bgWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorker_RunWorkerCompleted);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.prgStatus,
            this.lblStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 540);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(784, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(45, 17);
            this.toolStripStatusLabel1.Text = "Status :";
            // 
            // prgStatus
            // 
            this.prgStatus.Name = "prgStatus";
            this.prgStatus.Size = new System.Drawing.Size(100, 16);
            this.prgStatus.Value = 100;
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(39, 17);
            this.lblStatus.Text = "Ready";
            // 
            // csvDialog
            // 
            this.csvDialog.Filter = "CSV Files|*.csv|All Files|*.*";
            // 
            // frmTags
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.tcTags);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.statusStrip1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmTags";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tag(s) Data";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmTags_FormClosed);
            this.Shown += new System.EventHandler(this.frmTags_Shown);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton btnMinBig;
        private System.Windows.Forms.ToolStripButton btnMinSmall;
        private System.Windows.Forms.ToolStripButton btnPlusSmall;
        private System.Windows.Forms.ToolStripButton btnPlusBig;
        private System.Windows.Forms.ToolStripButton btnExit;
        private System.Windows.Forms.TabControl tcTags;
        private System.ComponentModel.BackgroundWorker bgWorker;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripProgressBar prgStatus;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.ToolStripSplitButton btnCSV;
        private System.Windows.Forms.ToolStripMenuItem mnExportToFolder;
        private System.Windows.Forms.ToolStripMenuItem mnAppend;
        private System.Windows.Forms.ToolStripMenuItem mnSideBySide;
        private System.Windows.Forms.ToolStripSeparator csvSeparator;
        private System.Windows.Forms.FolderBrowserDialog dlgFolder;
        private System.Windows.Forms.SaveFileDialog csvDialog;
    }
}