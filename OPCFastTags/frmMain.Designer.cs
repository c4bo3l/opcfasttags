﻿namespace OPCFastTags
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnAddTags = new System.Windows.Forms.ToolStripButton();
            this.btnClearTags = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnRun = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnExit = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnConfig = new System.Windows.Forms.ToolStripButton();
            this.propGrid = new System.Windows.Forms.PropertyGrid();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbBigDuration = new System.Windows.Forms.ComboBox();
            this.nmBigStep = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.cbSmallDuration = new System.Windows.Forms.ComboBox();
            this.nmSmallStep = new System.Windows.Forms.NumericUpDown();
            this.lbl4 = new System.Windows.Forms.Label();
            this.nmDecimalPoint = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.dtEnd = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtStart = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgvTags = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnDelSelected = new System.Windows.Forms.DataGridViewImageColumn();
            this.tagName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tagDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmBigStep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmSmallStep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmDecimalPoint)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTags)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddTags,
            this.btnClearTags,
            this.toolStripSeparator1,
            this.btnRun,
            this.toolStripSeparator2,
            this.btnExit,
            this.toolStripSeparator3,
            this.btnConfig});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1008, 70);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnAddTags
            // 
            this.btnAddTags.Image = ((System.Drawing.Image)(resources.GetObject("btnAddTags.Image")));
            this.btnAddTags.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddTags.Name = "btnAddTags";
            this.btnAddTags.Size = new System.Drawing.Size(69, 67);
            this.btnAddTags.Text = "Add Tag(s)";
            this.btnAddTags.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnAddTags.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnAddTags.Click += new System.EventHandler(this.btnAddTags_Click);
            // 
            // btnClearTags
            // 
            this.btnClearTags.Image = ((System.Drawing.Image)(resources.GetObject("btnClearTags.Image")));
            this.btnClearTags.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClearTags.Name = "btnClearTags";
            this.btnClearTags.Size = new System.Drawing.Size(66, 67);
            this.btnClearTags.Text = "Clear Tags";
            this.btnClearTags.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnClearTags.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnClearTags.Click += new System.EventHandler(this.btnClearTags_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 70);
            // 
            // btnRun
            // 
            this.btnRun.Enabled = false;
            this.btnRun.Image = ((System.Drawing.Image)(resources.GetObject("btnRun.Image")));
            this.btnRun.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(52, 67);
            this.btnRun.Text = "Run";
            this.btnRun.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnRun.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 70);
            // 
            // btnExit
            // 
            this.btnExit.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(52, 67);
            this.btnExit.Text = "Exit";
            this.btnExit.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 70);
            // 
            // btnConfig
            // 
            this.btnConfig.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnConfig.Image = ((System.Drawing.Image)(resources.GetObject("btnConfig.Image")));
            this.btnConfig.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnConfig.Name = "btnConfig";
            this.btnConfig.Size = new System.Drawing.Size(85, 67);
            this.btnConfig.Text = "Configuration";
            this.btnConfig.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnConfig.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnConfig.Click += new System.EventHandler(this.btnConfig_Click);
            // 
            // propGrid
            // 
            this.propGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propGrid.Location = new System.Drawing.Point(3, 19);
            this.propGrid.Margin = new System.Windows.Forms.Padding(4);
            this.propGrid.Name = "propGrid";
            this.propGrid.PropertySort = System.Windows.Forms.PropertySort.Categorized;
            this.propGrid.Size = new System.Drawing.Size(355, 491);
            this.propGrid.TabIndex = 2;
            this.propGrid.SelectedObjectsChanged += new System.EventHandler(this.propGrid_SelectedObjectsChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbBigDuration);
            this.groupBox1.Controls.Add(this.nmBigStep);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cbSmallDuration);
            this.groupBox1.Controls.Add(this.nmSmallStep);
            this.groupBox1.Controls.Add(this.lbl4);
            this.groupBox1.Controls.Add(this.nmDecimalPoint);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.dtEnd);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dtStart);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 70);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1008, 147);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Global Setting";
            // 
            // cbBigDuration
            // 
            this.cbBigDuration.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbBigDuration.FormattingEnabled = true;
            this.cbBigDuration.Location = new System.Drawing.Point(181, 112);
            this.cbBigDuration.Name = "cbBigDuration";
            this.cbBigDuration.Size = new System.Drawing.Size(121, 24);
            this.cbBigDuration.TabIndex = 11;
            // 
            // nmBigStep
            // 
            this.nmBigStep.Location = new System.Drawing.Point(125, 113);
            this.nmBigStep.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nmBigStep.Name = "nmBigStep";
            this.nmBigStep.Size = new System.Drawing.Size(50, 23);
            this.nmBigStep.TabIndex = 10;
            this.nmBigStep.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nmBigStep.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(56, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Big step";
            // 
            // cbSmallDuration
            // 
            this.cbSmallDuration.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSmallDuration.FormattingEnabled = true;
            this.cbSmallDuration.Location = new System.Drawing.Point(181, 83);
            this.cbSmallDuration.Name = "cbSmallDuration";
            this.cbSmallDuration.Size = new System.Drawing.Size(121, 24);
            this.cbSmallDuration.TabIndex = 8;
            // 
            // nmSmallStep
            // 
            this.nmSmallStep.Location = new System.Drawing.Point(125, 84);
            this.nmSmallStep.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nmSmallStep.Name = "nmSmallStep";
            this.nmSmallStep.Size = new System.Drawing.Size(50, 23);
            this.nmSmallStep.TabIndex = 7;
            this.nmSmallStep.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nmSmallStep.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // lbl4
            // 
            this.lbl4.AutoSize = true;
            this.lbl4.Location = new System.Drawing.Point(42, 86);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(73, 17);
            this.lbl4.TabIndex = 6;
            this.lbl4.Text = "Small step";
            // 
            // nmDecimalPoint
            // 
            this.nmDecimalPoint.Location = new System.Drawing.Point(125, 55);
            this.nmDecimalPoint.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nmDecimalPoint.Name = "nmDecimalPoint";
            this.nmDecimalPoint.Size = new System.Drawing.Size(37, 23);
            this.nmDecimalPoint.TabIndex = 5;
            this.nmDecimalPoint.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nmDecimalPoint.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Decimal Point(s)";
            // 
            // dtEnd
            // 
            this.dtEnd.CustomFormat = "MM/dd/yyyy HH:mm:ss ";
            this.dtEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtEnd.Location = new System.Drawing.Point(327, 26);
            this.dtEnd.Name = "dtEnd";
            this.dtEnd.Size = new System.Drawing.Size(156, 23);
            this.dtEnd.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(287, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "until";
            // 
            // dtStart
            // 
            this.dtStart.CustomFormat = "MM/dd/yyyy HH:mm:ss ";
            this.dtStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtStart.Location = new System.Drawing.Point(125, 26);
            this.dtStart.Name = "dtStart";
            this.dtStart.Size = new System.Drawing.Size(156, 23);
            this.dtStart.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Fetch data from";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgvTags);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 217);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(647, 513);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Selected Tag(s)";
            // 
            // dgvTags
            // 
            this.dgvTags.AllowUserToAddRows = false;
            this.dgvTags.AllowUserToDeleteRows = false;
            this.dgvTags.AllowUserToResizeColumns = false;
            this.dgvTags.AllowUserToResizeRows = false;
            this.dgvTags.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvTags.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTags.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvTags.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTags.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.btnDelSelected,
            this.tagName,
            this.tagDesc});
            this.dgvTags.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvTags.Location = new System.Drawing.Point(3, 19);
            this.dgvTags.Name = "dgvTags";
            this.dgvTags.RowHeadersVisible = false;
            this.dgvTags.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTags.Size = new System.Drawing.Size(641, 491);
            this.dgvTags.TabIndex = 0;
            this.dgvTags.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTags_CellClick);
            this.dgvTags.SelectionChanged += new System.EventHandler(this.dgvTags_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn1.HeaderText = "Name";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 200;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 205;
            // 
            // dataGridViewTextBoxColumn2
            // 
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewTextBoxColumn2.HeaderText = "Description";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 380;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 380;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.propGrid);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox3.Location = new System.Drawing.Point(647, 217);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(361, 513);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Tag Setting";
            // 
            // btnDelSelected
            // 
            this.btnDelSelected.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.btnDelSelected.HeaderText = "";
            this.btnDelSelected.Image = ((System.Drawing.Image)(resources.GetObject("btnDelSelected.Image")));
            this.btnDelSelected.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.btnDelSelected.MinimumWidth = 25;
            this.btnDelSelected.Name = "btnDelSelected";
            this.btnDelSelected.ToolTipText = "Delete this tag";
            this.btnDelSelected.Width = 25;
            // 
            // tagName
            // 
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.tagName.DefaultCellStyle = dataGridViewCellStyle2;
            this.tagName.HeaderText = "Name";
            this.tagName.MinimumWidth = 200;
            this.tagName.Name = "tagName";
            this.tagName.ReadOnly = true;
            // 
            // tagDesc
            // 
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.tagDesc.DefaultCellStyle = dataGridViewCellStyle3;
            this.tagDesc.HeaderText = "Description";
            this.tagDesc.MinimumWidth = 380;
            this.tagDesc.Name = "tagDesc";
            this.tagDesc.ReadOnly = true;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.toolStrip1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OPC Fast Tags";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Shown += new System.EventHandler(this.frmMain_Shown);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmBigStep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmSmallStep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmDecimalPoint)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTags)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnAddTags;
        private System.Windows.Forms.ToolStripButton btnClearTags;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnRun;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnConfig;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton btnExit;
        private System.Windows.Forms.PropertyGrid propGrid;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dtStart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtEnd;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgvTags;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.ComboBox cbBigDuration;
        private System.Windows.Forms.NumericUpDown nmBigStep;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbSmallDuration;
        private System.Windows.Forms.NumericUpDown nmSmallStep;
        private System.Windows.Forms.Label lbl4;
        private System.Windows.Forms.NumericUpDown nmDecimalPoint;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridViewImageColumn btnDelSelected;
        private System.Windows.Forms.DataGridViewTextBoxColumn tagName;
        private System.Windows.Forms.DataGridViewTextBoxColumn tagDesc;
    }
}

