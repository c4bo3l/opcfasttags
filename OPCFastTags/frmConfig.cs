﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OPCFastTags.Library.BLL;
using CabZFramework.WIN;
using OPCFastTags.Library;
using CabZFramework.Core;
using Xi.Contracts.Data;

namespace OPCFastTags
{
    public partial class frmConfig : Form
    {
        #region Properties
        private ConfigInfo _Config;
        private Processor _Proc;

        /*private BindingSource _ConfigSource;
        public BindingSource ConfigSource
        {
            get
            {
                if (_ConfigSource == null)
                {
                    _ConfigSource = new BindingSource();
                    _ConfigSource.DataSource = _Config;
                }

                return _ConfigSource;
            }
        }*/
        #endregion

        #region Constructor
        private frmConfig()
        {
            InitializeComponent();
        }

        public frmConfig(ConfigInfo config, Processor proc)
            : this()
        {
            _Config = config;
            //BindProcess();
            _Proc = proc;
            _Proc.DiscoveryResult += _Proc_DiscoveryResult;
        }

        public void ClearCBName()
        {
            if (cbName.InvokeRequired)
                cbName.Invoke(new frmTags.EmptyParameter(ClearCBName));
            else
                cbName.Items.Clear();
        }

        public delegate void DelegateString(string s);
        public void AddCBNameItem(string name)
        {
            if (cbName.InvokeRequired)
                cbName.Invoke(new DelegateString(AddCBNameItem), name);
            else
            {
                cbName.Items.Add(name);
                cbName.SelectedItem = _Config.server_name;
            }
        }

        public void _Proc_DiscoveryResult(Exception e, List<Xi.Contracts.Data.ServerEntry> servers)
        {
            try
            {
                ClearCBName();

                if (e != null)
                    throw e;

                if (servers != null && servers.Count > 0)
                {
                    foreach (ServerEntry s in servers)
                        AddCBNameItem(s.ServerDescription.ServerName);
                }
                else
                    throw new Exception("No server found");
            }
            catch (Exception ex)
            {
                CabZFramework.Core.CommonModule.ExceptionFunc(this.GetType().ToString(), "_Proc_DiscoveryResult", ex, true);
            }
            finally
            {
                EnableDisableToolstripButton(true);
                lblStatus.Text = "Finish";
                ChangeProgressType(false);
            }
        }
        #endregion

        #region Binding Process
        /*protected void BindProcess()
        {
            ObjectHelper.BindHelper<ConfigInfo.PropList>("text", ConfigSource, new Control[] { txtIP, txtName }, 
                new ConfigInfo.PropList[] { ConfigInfo.PropList.ip_server, ConfigInfo.PropList.server_name }, 
                DataSourceUpdateMode.OnPropertyChanged);
        }*/
        #endregion

        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void frmConfig_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
            GC.Collect();
        }

        private void frmConfig_Shown(object sender, EventArgs e)
        {
            try
            {
                if (System.IO.File.Exists(_Config.XMLPath))
                    _Config.FetchData();
                txtIP.Text = _Config.ip_server;
                btnBrowse_Click(this, null);
                //ConfigSource.ResetBindings(false);
            }
            catch (Exception ex)
            {
                CabZFramework.Core.CommonModule.ExceptionFunc(this.GetType().ToString(), "frmConfig_Shown", ex, true);
                this.Close();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                _Config.ip_server = txtIP.Text;
                _Config.server_name = cbName.SelectedItem.ToString();
                _Config.ProcessData();
                DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            catch(Exception ex)
            {
                CabZFramework.Core.CommonModule.ExceptionFunc(this.GetType().ToString(), "btnSave_Click", ex, true);
            }
        }

        private void btnTestConnection_Click(object sender, EventArgs e)
        {
            try
            {
                if (_Proc == null)
                    throw new ArgumentNullException("Processor is null");

                if (_Proc.TestConnection(txtIP.Text.Trim(),cbName.SelectedItem.ToString().Trim()))
                    VDialog.Show(this, "OPC server connected successfully", "Test Connection Result", 
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    VDialog.Show(this, "Failed to connect to OPC Server", "Test Connection Result",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                CabZFramework.Core.CommonModule.ExceptionFunc(this.GetType().ToString(), "btnTestConnection_Click", ex, true);
            }
        }

        public delegate void DelegateChangeProgress(bool isMarquee);
        public void ChangeProgressType(bool isMarquee)
        {
            if (prgStatus.ProgressBar != null)
            {
                if (prgStatus.ProgressBar.InvokeRequired)
                    prgStatus.ProgressBar.Invoke(new DelegateChangeProgress(ChangeProgressType), isMarquee);
                else
                {
                    if (isMarquee)
                    {
                        prgStatus.ProgressBar.Value = 10;
                        prgStatus.ProgressBar.Style = ProgressBarStyle.Marquee;

                    }
                    else
                    {
                        prgStatus.ProgressBar.Style = ProgressBarStyle.Continuous;
                        prgStatus.ProgressBar.Value = 100;
                    }
                }
            }
        }

        public void EnableDisableToolstripButton(bool enable)
        {
            if (toolStrip1.InvokeRequired)
                toolStrip1.Invoke(new frmTags.EnabledChange(EnableDisableToolstripButton), enable);
            else
                toolStrip1.Enabled = enable;
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtIP.Text.Trim().Length > 0)
                {
                    EnableDisableToolstripButton(false);
                    lblStatus.Text = string.Format("Discovering server on {0}", txtIP.Text);
                    ChangeProgressType(true);
                    _Proc.DiscoverServer(txtIP.Text.Trim());
                }
            }
            catch (Exception ex)
            {
                CabZFramework.Core.CommonModule.ExceptionFunc(this.GetType().ToString(), "btnBrowse_Click", ex, true);
            }
        }
    }
}
